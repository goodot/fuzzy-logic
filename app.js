Ext.application({
    name    : 'DMS',
    appFolder: 'app',
    requires: ['DMS.view.MainPanel'],


    launch: function () {
        Ext.create('Ext.container.Viewport', {
            layout: 'fit',
            items : Ext.create('DMS.view.MainPanel')
        });
    }
});
