/**
 * Created by mikheil on 1/14/17.
 */
Ext.define('DMS.store.Object', {
    extend: 'Ext.data.Store',
    fields: ['id', 'identifier', 'feature_values'],
    alias : 'store.object',

    data: [
        {id:1,identifier: "მანუ ჯინობილი", feature_values: [39, 198, 15]},
        {id:2,identifier: "ტონი პარკერი", feature_values: [35, 188, 16]},
        {id:3,identifier: "კავაი ლეონარდი", feature_values: [25, 201, 3]},
        {id:4,identifier: "ზაზა ფაჩულია", feature_values: [32, 211, 13]}
    ],

    autoload: true,
    autoSync: true

});