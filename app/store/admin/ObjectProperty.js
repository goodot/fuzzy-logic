/**
 * Created by mikheil on 11/27/16.
 */
Ext.define('DMS.store.admin.ObjectProperty', {
    extend: 'Ext.data.Store',
    fields: ['id', 'name', 'a', 'b', 'c'],
    alias : 'store.admin.objectproperty',

    data: [
        {id: 1, name: 'ასაკი', a: 10, b: 28, c: 45},
        {id: 2, name: 'სიმაღლე', a: 160, b: 215, c: 240},
        {id: 3, name: 'NBA-ში ჩატარებული სეზონების რაოდენობა', a: 0, b: 10, c: 20},
    ],

    autoload: true,
    autoSync: true

});