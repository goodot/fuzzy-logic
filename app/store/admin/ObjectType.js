/**
 * Created by mikheil on 11/27/16.
 */
Ext.define('DMS.store.admin.ObjectType', {
    extend: 'Ext.data.Store',
    fields: ['id', 'name'],
    alias : 'store.admin.objecttype',

    data: [{
        id: 1, name: 'კალათბურთელის არჩევა'
    }],

    //autoload: true,
    //autoSync: true


});