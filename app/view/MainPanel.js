/**
 * Created by mikheil on 11/26/16.
 */
Ext.define('DMS.view.MainPanel', {
    extend: 'Ext.tab.Panel',

    requires: [
        'DMS.view.admin.MainPanel',

        'DMS.view.admin.objectdescription.MainPanel',
        'DMS.view.admin.objectdescription.ObjectTypeGrid',
        'DMS.view.admin.objectdescription.ObjectPropertyGrid',

        'DMS.store.admin.ObjectType',

        'DMS.view.user.MainPanel',
        'DMS.view.user.components.TypeCombo',

        'DMS.view.user.components.ObjectGrid',
        'DMS.store.Object'
    ],

    xtype: 'dms-mainpanel',

    layout: 'border',

    items: [
        {
            xtype: 'dms-user-mainpanel',
            title: 'მონაცემების დამუშავება'
        }
        , {
            xtype: 'dms-admin-mainpanel',
            title: 'ადმინის გვერდი'
        }
    ]
});