/**
 * Created by mikheil on 1/13/17.
 */
Ext.define('DMS.view.user.MainPanel', {
    extend: 'Ext.panel.Panel',


    xtype: 'dms-user-mainpanel',

    layout: {
        type : 'vbox',
        align: 'stretch'
    },
    width : '100%',

    items: [
        {
            xtype       : 'dms-user-component-typecombo',
            fieldLabel  : 'ტიპის არჩევა',
            displayField: 'name',
            valueField  : 'id',
            margin      : 5,
            editable    : false,
            width       : '500px'
        },
        {
            xtype: 'dms-user-component-objectgrid',
            title: 'შესარჩევი ობიექტები'
        }
    ],
    bbar : [
        {
            xtype: 'button', text: 'ამორჩევა', style: {backgroundColor: 'lightGreen'}, handler: function () {

            var data = {
                "data": {
                    "name"       : "კალათბურთელების არჩევა",
                    "features"   : [
                        {
                            "identifier": "ასაკი",
                            "trimf"     : {
                                "a": 10,
                                "b": 28,
                                "c": 45
                            },
                            "weight"    : 0.7
                        },
                        {
                            "identifier": "სიმაღლე",
                            "trimf"     : {
                                "a": 160,
                                "b": 215,
                                "c": 240
                            },
                            "weight"    : 0.8
                        },
                        {
                            "identifier": "NBA-ში ჩატარებული სეზონების რაოდენობა",
                            "trimf"     : {
                                "a": 0,
                                "b": 10,
                                "c": 20
                            },
                            "weight"    : 1
                        }],
                    "items"      : [
                        {
                            "identifier"    : "მანუ ჯინობილი",
                            "feature_values": [39, 198, 15]
                        },
                        {
                            "identifier"    : "ტონი პარკერი",
                            "feature_values": [35, 188, 16]
                        },
                        {
                            "identifier"    : "კავაი ლეონარდი",
                            "feature_values": [25, 201, 3]
                        },
                        {
                            "identifier"    : "ზაზა ფაჩულია",
                            "feature_values": [32, 211, 13]
                        }
                    ],
                    "aggregation": "WEIGHTED_ARITHMETIC_MEAN"


                }
            }


            Ext.Ajax.request({
                url     : './fuzzy-analyzer.php',
                method  : 'GET',
                jsonData: data,
                success : function (response) {
                    Ext.Msg.alert('შეტყობინება', response.responseText);
                }
            });

        }
        }
    ]

});