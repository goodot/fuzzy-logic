/**
 * Created by mikheil on 1/14/17.
 */
Ext.define('DMS.view.user.components.ObjectGrid', {
    extend: 'Ext.grid.Panel',

    xtype: 'dms-user-component-objectgrid',

    store: {type: 'object'},

    columns: [
        {text: 'ტიპის დასახელება', dataIndex: 'identifier', flex: 1},
        {text: 'ტიპის დასახელება', dataIndex: 'feature_values', flex: 1},
    ],

    tools: [{
        type: 'plus',
        //handler:todo
    }]

});