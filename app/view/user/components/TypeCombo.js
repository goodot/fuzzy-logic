/**
 * Created by mikheil on 1/14/17.
 */
Ext.define('DMS.view.user.components.TypeCombo', {
    extend: 'Ext.form.field.ComboBox',


    xtype: 'dms-user-component-typecombo',

    store: {type: 'admin.objecttype'},


});