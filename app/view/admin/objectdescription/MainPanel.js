/**
 * Created by mikheil on 11/27/16.
 */
Ext.define('DMS.view.admin.objectdescription.MainPanel', {
    extend: 'Ext.panel.Panel',

    xtype: 'dms-admin-objdescr-mainpanel',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    items: [{
        title: 'ობიექტები',
        xtype: 'dms-admin-objdescr-typegrid',
        flex: 1
    }, {
        title: 'თვისებები',
        xtype: 'dms-admin-objdescr-propertygrid',
        flex: 3
    }]



});