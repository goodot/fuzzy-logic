/**
 * Created by mikheil on 11/27/16.
 */
Ext.define('DMS.view.admin.objectdescription.ObjectTypeGrid', {
    extend: 'Ext.grid.Panel',


    xtype: 'dms-admin-objdescr-typegrid',

    store: {type: 'admin.objecttype'},

    columns: [
        {text: 'ტიპის დასახელება', dataIndex: 'name', flex: 1},
    ],

    tools: [{
        type: 'plus',
        //handler:todo
    }]

});