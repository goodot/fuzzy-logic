/**
 * Created by mikheil on 11/27/16.
 */
Ext.define('DMS.view.admin.objectdescription.ObjectPropertyGrid', {
    extend: 'Ext.grid.Panel',

    requires: [
        'DMS.store.admin.ObjectProperty',
    ],

    xtype: 'dms-admin-objdescr-propertygrid',

    store: {type: 'admin.objectproperty'},

    columns: [
        {text: 'თვისების დასახელება', dataIndex: 'name', flex: 1},
        {text: 'A', dataIndex: 'a', flex: 1},
        {text: 'B', dataIndex: 'b', flex: 1},
        {text: 'C', dataIndex: 'c', flex: 1},
    ],


    plugins: {
        ptype: 'rowediting',
        id: 'admin.objectdescription.ObjectPropertyGrid.rowediting',
        clicksToEdit: 2
    },

    tools: [{
        type: 'plus',
        //handler:todo
    }]

});
