/**
 * Created by mikheil on 11/27/16.
 */
Ext.define('DMS.view.admin.MainPanel', {
    extend: 'Ext.panel.Panel',

    xtype: 'dms-admin-mainpanel',

    layout: 'fit',

    items: [{
        xtype: 'dms-admin-objdescr-mainpanel'
    }],
});