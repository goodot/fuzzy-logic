<?php

include("models/model-loader.php");



$f1 = new Feature("Height", new Trimf(160, 203, 220));
$f2 = new Feature("Age", new Trimf(10, 28, 45));
$features = array($f1, $f2);

$item1 = new Item("Manu Ginobili", array(198, 39));
$item2 = new Item("Lebron James", array(202, 33));

$items = array($item1, $item2);


$fuzzy_analyzer = new Analyzer($features, $items, new ArithmeticMean());

echo json_encode($fuzzy_analyzer->analyze());
